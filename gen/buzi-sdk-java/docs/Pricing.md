

# Pricing


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**costPerUnit** | **BigDecimal** |  |  [optional] |


## Implemented Interfaces

* Serializable


