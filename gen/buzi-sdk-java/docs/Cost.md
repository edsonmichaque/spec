

# Cost


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**currency** | **String** |  |  [optional] |
|**value** | **BigDecimal** |  |  [optional] |


## Implemented Interfaces

* Serializable


