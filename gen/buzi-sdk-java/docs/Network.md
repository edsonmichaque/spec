

# Network


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**id** | **Integer** |  |  [optional] |
|**createdAt** | **Integer** |  |  [optional] |


## Implemented Interfaces

* Serializable


