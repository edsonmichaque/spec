

# CreateMessageInput


## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
|**to** | **List&lt;String&gt;** |  |  [optional] |
|**from** | **String** |  |  [optional] |
|**networkId** | **Integer** |  |  [optional] |
|**callbackUrl** | **String** |  |  [optional] |


## Implemented Interfaces

* Serializable


