/*
 * Swagger Petstore - OpenAPI 3.0
 * This is a sample Pet Store Server based on the OpenAPI 3.0 specification.  You can find out more about Swagger at [https://swagger.io](https://swagger.io). In the third iteration of the pet store, we've switched to the design first approach! You can now help us improve the API whether it's by making changes to the definition itself or to the code. That way, with time, we can improve the API in general, and expose some of the new features in OAS3.  _If you're looking for the Swagger 2.0/OAS 2.0 version of Petstore, then click [here](https://editor.swagger.io/?url=https://petstore.swagger.io/v2/swagger.yaml). Alternatively, you can load via the `Edit > Load Petstore OAS 2.0` menu option!_  Some useful links: - [The Pet Store repository](https://github.com/swagger-api/swagger-petstore) - [The source API definition for the Pet Store](https://github.com/swagger-api/swagger-petstore/blob/master/src/main/resources/openapi.yaml)
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: edson@michaque.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package io.gitlab.buziproject.v2;

import io.gitlab.buziproject.v2.ApiException;
import io.gitlab.buziproject.v2.model.CreateMessageInput;
import io.gitlab.buziproject.v2.model.Error;
import io.gitlab.buziproject.v2.model.Message;
import io.gitlab.buziproject.v2.model.Network;
import io.gitlab.buziproject.v2.model.Pricing;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for SmsApi
 */
@Disabled
public class SmsApiTest {

    private final SmsApi api = new SmsApi();

    /**
     * Cancel a message
     *
     * Returns a single pet
     *
     * @throws ApiException if the Api call fails
     */
    @Test
    public void cancelMessageTest() throws ApiException {
        Long messageId = null;
        Message response = api.cancelMessage(messageId);
        // TODO: test validations
    }

    /**
     * Create Message
     *
     * Update an existing pet by Id
     *
     * @throws ApiException if the Api call fails
     */
    @Test
    public void createMessageTest() throws ApiException {
        CreateMessageInput createMessageInput = null;
        Message response = api.createMessage(createMessageInput);
        // TODO: test validations
    }

    /**
     * Create network price
     *
     * Returns a single pet
     *
     * @throws ApiException if the Api call fails
     */
    @Test
    public void createPricingTest() throws ApiException {
        Integer networkId = null;
        Message response = api.createPricing(networkId);
        // TODO: test validations
    }

    /**
     * Deletes a message
     *
     * delete a message
     *
     * @throws ApiException if the Api call fails
     */
    @Test
    public void deleteMessageTest() throws ApiException {
        Long messageId = null;
        String apiKey = null;
        Error response = api.deleteMessage(messageId, apiKey);
        // TODO: test validations
    }

    /**
     * Get message
     *
     * Returns a single pet
     *
     * @throws ApiException if the Api call fails
     */
    @Test
    public void getMessageTest() throws ApiException {
        Long messageId = null;
        Message response = api.getMessage(messageId);
        // TODO: test validations
    }

    /**
     * Get network
     *
     * Returns a single pet
     *
     * @throws ApiException if the Api call fails
     */
    @Test
    public void getNetworkTest() throws ApiException {
        Integer networkId = null;
        Long countryCode = null;
        Network response = api.getNetwork(networkId, countryCode);
        // TODO: test validations
    }

    /**
     * List network rates
     *
     * Returns a single pet
     *
     * @throws ApiException if the Api call fails
     */
    @Test
    public void getPricingTest() throws ApiException {
        Integer networkId = null;
        List<Pricing> response = api.getPricing(networkId);
        // TODO: test validations
    }

    /**
     * List messages
     *
     * Update an existing pet by Id
     *
     * @throws ApiException if the Api call fails
     */
    @Test
    public void listMessagesTest() throws ApiException {
        String inbox = null;
        String status = null;
        List<Message> response = api.listMessages(inbox, status);
        // TODO: test validations
    }

    /**
     * List networks
     *
     * Returns a single pet
     *
     * @throws ApiException if the Api call fails
     */
    @Test
    public void listNetworksTest() throws ApiException {
        String countryCode = null;
        List<Network> response = api.listNetworks(countryCode);
        // TODO: test validations
    }

    /**
     * Sends a message
     *
     * Returns a single pet
     *
     * @throws ApiException if the Api call fails
     */
    @Test
    public void sendMessageTest() throws ApiException {
        Long messageId = null;
        Message response = api.sendMessage(messageId);
        // TODO: test validations
    }

}
