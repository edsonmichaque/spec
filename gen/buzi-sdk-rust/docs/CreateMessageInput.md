# CreateMessageInput

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**to** | Option<**Vec<String>**> |  | [optional]
**from** | Option<**String**> |  | [optional]
**network_id** | Option<**i32**> |  | [optional]
**callback_url** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


