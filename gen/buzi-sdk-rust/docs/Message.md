# Message

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<[**uuid::Uuid**](uuid::Uuid.md)> |  | [optional]
**created_at** | Option<**String**> |  | [optional]
**status** | Option<**String**> |  | [optional]
**reason** | Option<**String**> |  | [optional]
**cost** | Option<[**crate::models::Cost**](Cost.md)> |  | [optional]
**inbox** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


