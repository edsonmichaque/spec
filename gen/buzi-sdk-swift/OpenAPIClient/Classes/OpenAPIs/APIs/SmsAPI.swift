//
// SmsAPI.swift
//
// Generated by openapi-generator
// https://openapi-generator.tech
//

import Foundation
#if canImport(AnyCodable)
import AnyCodable
#endif

open class SmsAPI {

    /**
     Cancel a message
     
     - parameter messageId: (path) ID of pet to return 
     - parameter apiResponseQueue: The queue on which api response is dispatched.
     - parameter completion: completion handler to receive the data and the error objects
     */
    @discardableResult
    open class func cancelMessage(messageId: Int64, apiResponseQueue: DispatchQueue = OpenAPIClientAPI.apiResponseQueue, completion: @escaping ((_ data: Message?, _ error: Error?) -> Void)) -> RequestTask {
        return cancelMessageWithRequestBuilder(messageId: messageId).execute(apiResponseQueue) { result in
            switch result {
            case let .success(response):
                completion(response.body, nil)
            case let .failure(error):
                completion(nil, error)
            }
        }
    }

    /**
     Cancel a message
     - POST /v1/sms/messages/{messageId}/cancel
     - Returns a single pet
     - API Key:
       - type: apiKey X-API-Key 
       - name: ApiKeyAuth
     - BASIC:
       - type: http
       - name: BasicAuth
     - BASIC:
       - type: http
       - name: BearerAuth
     - parameter messageId: (path) ID of pet to return 
     - returns: RequestBuilder<Message> 
     */
    open class func cancelMessageWithRequestBuilder(messageId: Int64) -> RequestBuilder<Message> {
        var localVariablePath = "/v1/sms/messages/{messageId}/cancel"
        let messageIdPreEscape = "\(APIHelper.mapValueToPathItem(messageId))"
        let messageIdPostEscape = messageIdPreEscape.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? ""
        localVariablePath = localVariablePath.replacingOccurrences(of: "{messageId}", with: messageIdPostEscape, options: .literal, range: nil)
        let localVariableURLString = OpenAPIClientAPI.basePath + localVariablePath
        let localVariableParameters: [String: Any]? = nil

        let localVariableUrlComponents = URLComponents(string: localVariableURLString)

        let localVariableNillableHeaders: [String: Any?] = [
            :
        ]

        let localVariableHeaderParameters = APIHelper.rejectNilHeaders(localVariableNillableHeaders)

        let localVariableRequestBuilder: RequestBuilder<Message>.Type = OpenAPIClientAPI.requestBuilderFactory.getBuilder()

        return localVariableRequestBuilder.init(method: "POST", URLString: (localVariableUrlComponents?.string ?? localVariableURLString), parameters: localVariableParameters, headers: localVariableHeaderParameters, requiresAuthentication: truetruetrue)
    }

    /**
     Create Message
     
     - parameter createMessageInput: (body) Update an existent pet in the store 
     - parameter apiResponseQueue: The queue on which api response is dispatched.
     - parameter completion: completion handler to receive the data and the error objects
     */
    @discardableResult
    open class func createMessage(createMessageInput: CreateMessageInput, apiResponseQueue: DispatchQueue = OpenAPIClientAPI.apiResponseQueue, completion: @escaping ((_ data: Message?, _ error: Error?) -> Void)) -> RequestTask {
        return createMessageWithRequestBuilder(createMessageInput: createMessageInput).execute(apiResponseQueue) { result in
            switch result {
            case let .success(response):
                completion(response.body, nil)
            case let .failure(error):
                completion(nil, error)
            }
        }
    }

    /**
     Create Message
     - POST /v1/sms/messages
     - Update an existing pet by Id
     - API Key:
       - type: apiKey X-API-Key 
       - name: ApiKeyAuth
     - BASIC:
       - type: http
       - name: BasicAuth
     - BASIC:
       - type: http
       - name: BearerAuth
     - parameter createMessageInput: (body) Update an existent pet in the store 
     - returns: RequestBuilder<Message> 
     */
    open class func createMessageWithRequestBuilder(createMessageInput: CreateMessageInput) -> RequestBuilder<Message> {
        let localVariablePath = "/v1/sms/messages"
        let localVariableURLString = OpenAPIClientAPI.basePath + localVariablePath
        let localVariableParameters = JSONEncodingHelper.encodingParameters(forEncodableObject: createMessageInput)

        let localVariableUrlComponents = URLComponents(string: localVariableURLString)

        let localVariableNillableHeaders: [String: Any?] = [
            :
        ]

        let localVariableHeaderParameters = APIHelper.rejectNilHeaders(localVariableNillableHeaders)

        let localVariableRequestBuilder: RequestBuilder<Message>.Type = OpenAPIClientAPI.requestBuilderFactory.getBuilder()

        return localVariableRequestBuilder.init(method: "POST", URLString: (localVariableUrlComponents?.string ?? localVariableURLString), parameters: localVariableParameters, headers: localVariableHeaderParameters, requiresAuthentication: truetruetrue)
    }

    /**
     Create network price
     
     - parameter networkId: (path)  
     - parameter apiResponseQueue: The queue on which api response is dispatched.
     - parameter completion: completion handler to receive the data and the error objects
     */
    @discardableResult
    open class func createPricing(networkId: Int, apiResponseQueue: DispatchQueue = OpenAPIClientAPI.apiResponseQueue, completion: @escaping ((_ data: Message?, _ error: Error?) -> Void)) -> RequestTask {
        return createPricingWithRequestBuilder(networkId: networkId).execute(apiResponseQueue) { result in
            switch result {
            case let .success(response):
                completion(response.body, nil)
            case let .failure(error):
                completion(nil, error)
            }
        }
    }

    /**
     Create network price
     - PUT /v1/sms/networks/{networkId}/pricing
     - Returns a single pet
     - API Key:
       - type: apiKey X-API-Key 
       - name: ApiKeyAuth
     - BASIC:
       - type: http
       - name: BasicAuth
     - BASIC:
       - type: http
       - name: BearerAuth
     - parameter networkId: (path)  
     - returns: RequestBuilder<Message> 
     */
    open class func createPricingWithRequestBuilder(networkId: Int) -> RequestBuilder<Message> {
        var localVariablePath = "/v1/sms/networks/{networkId}/pricing"
        let networkIdPreEscape = "\(APIHelper.mapValueToPathItem(networkId))"
        let networkIdPostEscape = networkIdPreEscape.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? ""
        localVariablePath = localVariablePath.replacingOccurrences(of: "{networkId}", with: networkIdPostEscape, options: .literal, range: nil)
        let localVariableURLString = OpenAPIClientAPI.basePath + localVariablePath
        let localVariableParameters: [String: Any]? = nil

        let localVariableUrlComponents = URLComponents(string: localVariableURLString)

        let localVariableNillableHeaders: [String: Any?] = [
            :
        ]

        let localVariableHeaderParameters = APIHelper.rejectNilHeaders(localVariableNillableHeaders)

        let localVariableRequestBuilder: RequestBuilder<Message>.Type = OpenAPIClientAPI.requestBuilderFactory.getBuilder()

        return localVariableRequestBuilder.init(method: "PUT", URLString: (localVariableUrlComponents?.string ?? localVariableURLString), parameters: localVariableParameters, headers: localVariableHeaderParameters, requiresAuthentication: truetruetrue)
    }

    /**
     Deletes a message
     
     - parameter messageId: (path) Pet id to delete 
     - parameter apiKey: (header)  (optional)
     - parameter apiResponseQueue: The queue on which api response is dispatched.
     - parameter completion: completion handler to receive the data and the error objects
     */
    @discardableResult
    open class func deleteMessage(messageId: Int64, apiKey: String? = nil, apiResponseQueue: DispatchQueue = OpenAPIClientAPI.apiResponseQueue, completion: @escaping ((_ data: ModelError?, _ error: Error?) -> Void)) -> RequestTask {
        return deleteMessageWithRequestBuilder(messageId: messageId, apiKey: apiKey).execute(apiResponseQueue) { result in
            switch result {
            case let .success(response):
                completion(response.body, nil)
            case let .failure(error):
                completion(nil, error)
            }
        }
    }

    /**
     Deletes a message
     - DELETE /v1/sms/messages/{messageId}
     - delete a message
     - API Key:
       - type: apiKey X-API-Key 
       - name: ApiKeyAuth
     - BASIC:
       - type: http
       - name: BasicAuth
     - BASIC:
       - type: http
       - name: BearerAuth
     - parameter messageId: (path) Pet id to delete 
     - parameter apiKey: (header)  (optional)
     - returns: RequestBuilder<ModelError> 
     */
    open class func deleteMessageWithRequestBuilder(messageId: Int64, apiKey: String? = nil) -> RequestBuilder<ModelError> {
        var localVariablePath = "/v1/sms/messages/{messageId}"
        let messageIdPreEscape = "\(APIHelper.mapValueToPathItem(messageId))"
        let messageIdPostEscape = messageIdPreEscape.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? ""
        localVariablePath = localVariablePath.replacingOccurrences(of: "{messageId}", with: messageIdPostEscape, options: .literal, range: nil)
        let localVariableURLString = OpenAPIClientAPI.basePath + localVariablePath
        let localVariableParameters: [String: Any]? = nil

        let localVariableUrlComponents = URLComponents(string: localVariableURLString)

        let localVariableNillableHeaders: [String: Any?] = [
            "api_key": apiKey?.encodeToJSON(),
        ]

        let localVariableHeaderParameters = APIHelper.rejectNilHeaders(localVariableNillableHeaders)

        let localVariableRequestBuilder: RequestBuilder<ModelError>.Type = OpenAPIClientAPI.requestBuilderFactory.getBuilder()

        return localVariableRequestBuilder.init(method: "DELETE", URLString: (localVariableUrlComponents?.string ?? localVariableURLString), parameters: localVariableParameters, headers: localVariableHeaderParameters, requiresAuthentication: truetruetrue)
    }

    /**
     Get message
     
     - parameter messageId: (path) ID of pet to return 
     - parameter apiResponseQueue: The queue on which api response is dispatched.
     - parameter completion: completion handler to receive the data and the error objects
     */
    @discardableResult
    open class func getMessage(messageId: Int64, apiResponseQueue: DispatchQueue = OpenAPIClientAPI.apiResponseQueue, completion: @escaping ((_ data: Message?, _ error: Error?) -> Void)) -> RequestTask {
        return getMessageWithRequestBuilder(messageId: messageId).execute(apiResponseQueue) { result in
            switch result {
            case let .success(response):
                completion(response.body, nil)
            case let .failure(error):
                completion(nil, error)
            }
        }
    }

    /**
     Get message
     - GET /v1/sms/messages/{messageId}
     - Returns a single pet
     - API Key:
       - type: apiKey X-API-Key 
       - name: ApiKeyAuth
     - BASIC:
       - type: http
       - name: BasicAuth
     - BASIC:
       - type: http
       - name: BearerAuth
     - parameter messageId: (path) ID of pet to return 
     - returns: RequestBuilder<Message> 
     */
    open class func getMessageWithRequestBuilder(messageId: Int64) -> RequestBuilder<Message> {
        var localVariablePath = "/v1/sms/messages/{messageId}"
        let messageIdPreEscape = "\(APIHelper.mapValueToPathItem(messageId))"
        let messageIdPostEscape = messageIdPreEscape.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? ""
        localVariablePath = localVariablePath.replacingOccurrences(of: "{messageId}", with: messageIdPostEscape, options: .literal, range: nil)
        let localVariableURLString = OpenAPIClientAPI.basePath + localVariablePath
        let localVariableParameters: [String: Any]? = nil

        let localVariableUrlComponents = URLComponents(string: localVariableURLString)

        let localVariableNillableHeaders: [String: Any?] = [
            :
        ]

        let localVariableHeaderParameters = APIHelper.rejectNilHeaders(localVariableNillableHeaders)

        let localVariableRequestBuilder: RequestBuilder<Message>.Type = OpenAPIClientAPI.requestBuilderFactory.getBuilder()

        return localVariableRequestBuilder.init(method: "GET", URLString: (localVariableUrlComponents?.string ?? localVariableURLString), parameters: localVariableParameters, headers: localVariableHeaderParameters, requiresAuthentication: truetruetrue)
    }

    /**
     Get network
     
     - parameter networkId: (path)  
     - parameter countryCode: (query) ID of pet to return (optional)
     - parameter apiResponseQueue: The queue on which api response is dispatched.
     - parameter completion: completion handler to receive the data and the error objects
     */
    @discardableResult
    open class func getNetwork(networkId: Int, countryCode: Int64? = nil, apiResponseQueue: DispatchQueue = OpenAPIClientAPI.apiResponseQueue, completion: @escaping ((_ data: Network?, _ error: Error?) -> Void)) -> RequestTask {
        return getNetworkWithRequestBuilder(networkId: networkId, countryCode: countryCode).execute(apiResponseQueue) { result in
            switch result {
            case let .success(response):
                completion(response.body, nil)
            case let .failure(error):
                completion(nil, error)
            }
        }
    }

    /**
     Get network
     - GET /v1/sms/networks/{networkId}
     - Returns a single pet
     - API Key:
       - type: apiKey X-API-Key 
       - name: ApiKeyAuth
     - BASIC:
       - type: http
       - name: BasicAuth
     - BASIC:
       - type: http
       - name: BearerAuth
     - parameter networkId: (path)  
     - parameter countryCode: (query) ID of pet to return (optional)
     - returns: RequestBuilder<Network> 
     */
    open class func getNetworkWithRequestBuilder(networkId: Int, countryCode: Int64? = nil) -> RequestBuilder<Network> {
        var localVariablePath = "/v1/sms/networks/{networkId}"
        let networkIdPreEscape = "\(APIHelper.mapValueToPathItem(networkId))"
        let networkIdPostEscape = networkIdPreEscape.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? ""
        localVariablePath = localVariablePath.replacingOccurrences(of: "{networkId}", with: networkIdPostEscape, options: .literal, range: nil)
        let localVariableURLString = OpenAPIClientAPI.basePath + localVariablePath
        let localVariableParameters: [String: Any]? = nil

        var localVariableUrlComponents = URLComponents(string: localVariableURLString)
        localVariableUrlComponents?.queryItems = APIHelper.mapValuesToQueryItems([
            "country_code": (wrappedValue: countryCode?.encodeToJSON(), isExplode: true),
        ])

        let localVariableNillableHeaders: [String: Any?] = [
            :
        ]

        let localVariableHeaderParameters = APIHelper.rejectNilHeaders(localVariableNillableHeaders)

        let localVariableRequestBuilder: RequestBuilder<Network>.Type = OpenAPIClientAPI.requestBuilderFactory.getBuilder()

        return localVariableRequestBuilder.init(method: "GET", URLString: (localVariableUrlComponents?.string ?? localVariableURLString), parameters: localVariableParameters, headers: localVariableHeaderParameters, requiresAuthentication: truetruetrue)
    }

    /**
     List network rates
     
     - parameter networkId: (path)  
     - parameter apiResponseQueue: The queue on which api response is dispatched.
     - parameter completion: completion handler to receive the data and the error objects
     */
    @discardableResult
    open class func getPricing(networkId: Int, apiResponseQueue: DispatchQueue = OpenAPIClientAPI.apiResponseQueue, completion: @escaping ((_ data: [Pricing]?, _ error: Error?) -> Void)) -> RequestTask {
        return getPricingWithRequestBuilder(networkId: networkId).execute(apiResponseQueue) { result in
            switch result {
            case let .success(response):
                completion(response.body, nil)
            case let .failure(error):
                completion(nil, error)
            }
        }
    }

    /**
     List network rates
     - GET /v1/sms/networks/{networkId}/pricing
     - Returns a single pet
     - API Key:
       - type: apiKey X-API-Key 
       - name: ApiKeyAuth
     - BASIC:
       - type: http
       - name: BasicAuth
     - BASIC:
       - type: http
       - name: BearerAuth
     - parameter networkId: (path)  
     - returns: RequestBuilder<[Pricing]> 
     */
    open class func getPricingWithRequestBuilder(networkId: Int) -> RequestBuilder<[Pricing]> {
        var localVariablePath = "/v1/sms/networks/{networkId}/pricing"
        let networkIdPreEscape = "\(APIHelper.mapValueToPathItem(networkId))"
        let networkIdPostEscape = networkIdPreEscape.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? ""
        localVariablePath = localVariablePath.replacingOccurrences(of: "{networkId}", with: networkIdPostEscape, options: .literal, range: nil)
        let localVariableURLString = OpenAPIClientAPI.basePath + localVariablePath
        let localVariableParameters: [String: Any]? = nil

        let localVariableUrlComponents = URLComponents(string: localVariableURLString)

        let localVariableNillableHeaders: [String: Any?] = [
            :
        ]

        let localVariableHeaderParameters = APIHelper.rejectNilHeaders(localVariableNillableHeaders)

        let localVariableRequestBuilder: RequestBuilder<[Pricing]>.Type = OpenAPIClientAPI.requestBuilderFactory.getBuilder()

        return localVariableRequestBuilder.init(method: "GET", URLString: (localVariableUrlComponents?.string ?? localVariableURLString), parameters: localVariableParameters, headers: localVariableHeaderParameters, requiresAuthentication: truetruetrue)
    }

    /**
     List messages
     
     - parameter inbox: (query)  (optional)
     - parameter status: (query)  (optional)
     - parameter apiResponseQueue: The queue on which api response is dispatched.
     - parameter completion: completion handler to receive the data and the error objects
     */
    @discardableResult
    open class func listMessages(inbox: String? = nil, status: String? = nil, apiResponseQueue: DispatchQueue = OpenAPIClientAPI.apiResponseQueue, completion: @escaping ((_ data: [Message]?, _ error: Error?) -> Void)) -> RequestTask {
        return listMessagesWithRequestBuilder(inbox: inbox, status: status).execute(apiResponseQueue) { result in
            switch result {
            case let .success(response):
                completion(response.body, nil)
            case let .failure(error):
                completion(nil, error)
            }
        }
    }

    /**
     List messages
     - GET /v1/sms/messages
     - Update an existing pet by Id
     - API Key:
       - type: apiKey X-API-Key 
       - name: ApiKeyAuth
     - BASIC:
       - type: http
       - name: BasicAuth
     - BASIC:
       - type: http
       - name: BearerAuth
     - parameter inbox: (query)  (optional)
     - parameter status: (query)  (optional)
     - returns: RequestBuilder<[Message]> 
     */
    open class func listMessagesWithRequestBuilder(inbox: String? = nil, status: String? = nil) -> RequestBuilder<[Message]> {
        let localVariablePath = "/v1/sms/messages"
        let localVariableURLString = OpenAPIClientAPI.basePath + localVariablePath
        let localVariableParameters: [String: Any]? = nil

        var localVariableUrlComponents = URLComponents(string: localVariableURLString)
        localVariableUrlComponents?.queryItems = APIHelper.mapValuesToQueryItems([
            "inbox": (wrappedValue: inbox?.encodeToJSON(), isExplode: true),
            "status": (wrappedValue: status?.encodeToJSON(), isExplode: true),
        ])

        let localVariableNillableHeaders: [String: Any?] = [
            :
        ]

        let localVariableHeaderParameters = APIHelper.rejectNilHeaders(localVariableNillableHeaders)

        let localVariableRequestBuilder: RequestBuilder<[Message]>.Type = OpenAPIClientAPI.requestBuilderFactory.getBuilder()

        return localVariableRequestBuilder.init(method: "GET", URLString: (localVariableUrlComponents?.string ?? localVariableURLString), parameters: localVariableParameters, headers: localVariableHeaderParameters, requiresAuthentication: truetruetrue)
    }

    /**
     List networks
     
     - parameter countryCode: (query) ID of pet to return (optional)
     - parameter apiResponseQueue: The queue on which api response is dispatched.
     - parameter completion: completion handler to receive the data and the error objects
     */
    @discardableResult
    open class func listNetworks(countryCode: String? = nil, apiResponseQueue: DispatchQueue = OpenAPIClientAPI.apiResponseQueue, completion: @escaping ((_ data: [Network]?, _ error: Error?) -> Void)) -> RequestTask {
        return listNetworksWithRequestBuilder(countryCode: countryCode).execute(apiResponseQueue) { result in
            switch result {
            case let .success(response):
                completion(response.body, nil)
            case let .failure(error):
                completion(nil, error)
            }
        }
    }

    /**
     List networks
     - GET /v1/sms/networks
     - Returns a single pet
     - API Key:
       - type: apiKey X-API-Key 
       - name: ApiKeyAuth
     - BASIC:
       - type: http
       - name: BasicAuth
     - BASIC:
       - type: http
       - name: BearerAuth
     - parameter countryCode: (query) ID of pet to return (optional)
     - returns: RequestBuilder<[Network]> 
     */
    open class func listNetworksWithRequestBuilder(countryCode: String? = nil) -> RequestBuilder<[Network]> {
        let localVariablePath = "/v1/sms/networks"
        let localVariableURLString = OpenAPIClientAPI.basePath + localVariablePath
        let localVariableParameters: [String: Any]? = nil

        var localVariableUrlComponents = URLComponents(string: localVariableURLString)
        localVariableUrlComponents?.queryItems = APIHelper.mapValuesToQueryItems([
            "country_code": (wrappedValue: countryCode?.encodeToJSON(), isExplode: true),
        ])

        let localVariableNillableHeaders: [String: Any?] = [
            :
        ]

        let localVariableHeaderParameters = APIHelper.rejectNilHeaders(localVariableNillableHeaders)

        let localVariableRequestBuilder: RequestBuilder<[Network]>.Type = OpenAPIClientAPI.requestBuilderFactory.getBuilder()

        return localVariableRequestBuilder.init(method: "GET", URLString: (localVariableUrlComponents?.string ?? localVariableURLString), parameters: localVariableParameters, headers: localVariableHeaderParameters, requiresAuthentication: truetruetrue)
    }

    /**
     Sends a message
     
     - parameter messageId: (path) ID of pet to return 
     - parameter apiResponseQueue: The queue on which api response is dispatched.
     - parameter completion: completion handler to receive the data and the error objects
     */
    @discardableResult
    open class func sendMessage(messageId: Int64, apiResponseQueue: DispatchQueue = OpenAPIClientAPI.apiResponseQueue, completion: @escaping ((_ data: Message?, _ error: Error?) -> Void)) -> RequestTask {
        return sendMessageWithRequestBuilder(messageId: messageId).execute(apiResponseQueue) { result in
            switch result {
            case let .success(response):
                completion(response.body, nil)
            case let .failure(error):
                completion(nil, error)
            }
        }
    }

    /**
     Sends a message
     - POST /v1/sms/messages/{messageId}/send
     - Returns a single pet
     - API Key:
       - type: apiKey X-API-Key 
       - name: ApiKeyAuth
     - BASIC:
       - type: http
       - name: BasicAuth
     - BASIC:
       - type: http
       - name: BearerAuth
     - parameter messageId: (path) ID of pet to return 
     - returns: RequestBuilder<Message> 
     */
    open class func sendMessageWithRequestBuilder(messageId: Int64) -> RequestBuilder<Message> {
        var localVariablePath = "/v1/sms/messages/{messageId}/send"
        let messageIdPreEscape = "\(APIHelper.mapValueToPathItem(messageId))"
        let messageIdPostEscape = messageIdPreEscape.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed) ?? ""
        localVariablePath = localVariablePath.replacingOccurrences(of: "{messageId}", with: messageIdPostEscape, options: .literal, range: nil)
        let localVariableURLString = OpenAPIClientAPI.basePath + localVariablePath
        let localVariableParameters: [String: Any]? = nil

        let localVariableUrlComponents = URLComponents(string: localVariableURLString)

        let localVariableNillableHeaders: [String: Any?] = [
            :
        ]

        let localVariableHeaderParameters = APIHelper.rejectNilHeaders(localVariableNillableHeaders)

        let localVariableRequestBuilder: RequestBuilder<Message>.Type = OpenAPIClientAPI.requestBuilderFactory.getBuilder()

        return localVariableRequestBuilder.init(method: "POST", URLString: (localVariableUrlComponents?.string ?? localVariableURLString), parameters: localVariableParameters, headers: localVariableHeaderParameters, requiresAuthentication: truetruetrue)
    }
}
