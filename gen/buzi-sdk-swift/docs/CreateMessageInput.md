# CreateMessageInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**to** | **[String]** |  | [optional] 
**from** | **String** |  | [optional] 
**networkId** | **Int** |  | [optional] 
**callbackUrl** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


