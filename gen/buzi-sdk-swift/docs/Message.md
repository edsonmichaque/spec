# Message

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **UUID** |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**status** | **String** |  | [optional] 
**reason** | **String** |  | [optional] 
**cost** | [**Cost**](Cost.md) |  | [optional] 
**inbox** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


