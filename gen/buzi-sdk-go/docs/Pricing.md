# Pricing

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CostPerUnit** | Pointer to **float32** |  | [optional] 

## Methods

### NewPricingWithDefaults

`func NewPricingWithDefaults() *Pricing`

NewPricingWithDefaults instantiates a new Pricing object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCostPerUnit

`func (o *Pricing) GetCostPerUnit() float32`

GetCostPerUnit returns the CostPerUnit field if non-nil, zero value otherwise.

### GetCostPerUnitOk

`func (o *Pricing) GetCostPerUnitOk() (*float32, bool)`

GetCostPerUnitOk returns a tuple with the CostPerUnit field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCostPerUnit

`func (o *Pricing) SetCostPerUnit(v float32)`

SetCostPerUnit sets CostPerUnit field to given value.

### HasCostPerUnit

`func (o *Pricing) HasCostPerUnit() bool`

HasCostPerUnit returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


