/*
 * Swagger Petstore - OpenAPI 3.0
 *
 * This is a sample Pet Store Server based on the OpenAPI 3.0 specification.  You can find out more about Swagger at [https://swagger.io](https://swagger.io). In the third iteration of the pet store, we've switched to the design first approach! You can now help us improve the API whether it's by making changes to the definition itself or to the code. That way, with time, we can improve the API in general, and expose some of the new features in OAS3.  _If you're looking for the Swagger 2.0/OAS 2.0 version of Petstore, then click [here](https://editor.swagger.io/?url=https://petstore.swagger.io/v2/swagger.yaml). Alternatively, you can load via the `Edit > Load Petstore OAS 2.0` menu option!_  Some useful links: - [The Pet Store repository](https://github.com/swagger-api/swagger-petstore) - [The source API definition for the Pet Store](https://github.com/swagger-api/swagger-petstore/blob/master/src/main/resources/openapi.yaml)
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: edson@michaque.com
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using Xunit;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using Io.Gitlab.Buziproject.V1.Api;
using Io.Gitlab.Buziproject.V1.Model;
using Io.Gitlab.Buziproject.V1.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace Io.Gitlab.Buziproject.V1.Test.Model
{
    /// <summary>
    ///  Class for testing Pricing
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by OpenAPI Generator (https://openapi-generator.tech).
    /// Please update the test case below to test the model.
    /// </remarks>
    public class PricingTests : IDisposable
    {
        // TODO uncomment below to declare an instance variable for Pricing
        //private Pricing instance;

        public PricingTests()
        {
            // TODO uncomment below to create an instance of Pricing
            //instance = new Pricing();
        }

        public void Dispose()
        {
            // Cleanup when everything is done.
        }

        /// <summary>
        /// Test an instance of Pricing
        /// </summary>
        [Fact]
        public void PricingInstanceTest()
        {
            // TODO uncomment below to test "IsType" Pricing
            //Assert.IsType<Pricing>(instance);
        }


        /// <summary>
        /// Test the property 'CostPerUnit'
        /// </summary>
        [Fact]
        public void CostPerUnitTest()
        {
            // TODO unit test for the property 'CostPerUnit'
        }

    }

}
