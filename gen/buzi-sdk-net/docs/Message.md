# Io.Gitlab.Buziproject.V1.Model.Message

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **Guid** |  | [optional] 
**CreatedAt** | **DateTime** |  | [optional] 
**Status** | **string** |  | [optional] 
**Reason** | **string** |  | [optional] 
**Cost** | [**Cost**](Cost.md) |  | [optional] 
**Inbox** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

