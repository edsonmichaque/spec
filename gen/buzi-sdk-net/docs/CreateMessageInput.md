# Io.Gitlab.Buziproject.V1.Model.CreateMessageInput

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**To** | **List&lt;string&gt;** |  | [optional] 
**From** | **string** |  | [optional] 
**NetworkId** | **int** |  | [optional] 
**CallbackUrl** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

