# buzi.Message

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**createdAt** | **Date** |  | [optional] 
**status** | **String** |  | [optional] 
**reason** | **String** |  | [optional] 
**cost** | [**Cost**](Cost.md) |  | [optional] 
**inbox** | **String** |  | [optional] 



## Enum: StatusEnum


* `PENDING` (value: `"PENDING"`)

* `REJECTED` (value: `"REJECTED"`)





## Enum: InboxEnum


* `incoming` (value: `"incoming"`)

* `outgoing` (value: `"outgoing"`)




