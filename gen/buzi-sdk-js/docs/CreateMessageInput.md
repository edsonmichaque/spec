# buzi.CreateMessageInput

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**to** | **[String]** |  | [optional] 
**from** | **String** |  | [optional] 
**networkId** | **Number** |  | [optional] 
**callbackUrl** | **String** |  | [optional] 


