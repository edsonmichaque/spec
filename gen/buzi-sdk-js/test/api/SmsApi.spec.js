/**
 * Swagger Petstore - OpenAPI 3.0
 * This is a sample Pet Store Server based on the OpenAPI 3.0 specification.  You can find out more about Swagger at [https://swagger.io](https://swagger.io). In the third iteration of the pet store, we've switched to the design first approach! You can now help us improve the API whether it's by making changes to the definition itself or to the code. That way, with time, we can improve the API in general, and expose some of the new features in OAS3.  _If you're looking for the Swagger 2.0/OAS 2.0 version of Petstore, then click [here](https://editor.swagger.io/?url=https://petstore.swagger.io/v2/swagger.yaml). Alternatively, you can load via the `Edit > Load Petstore OAS 2.0` menu option!_  Some useful links: - [The Pet Store repository](https://github.com/swagger-api/swagger-petstore) - [The source API definition for the Pet Store](https://github.com/swagger-api/swagger-petstore/blob/master/src/main/resources/openapi.yaml)
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: edson@michaque.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', process.cwd()+'/src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require(process.cwd()+'/src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.buzi);
  }
}(this, function(expect, buzi) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new buzi.SmsApi();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('SmsApi', function() {
    describe('cancelMessage', function() {
      it('should call cancelMessage successfully', function(done) {
        //uncomment below and update the code to test cancelMessage
        //instance.cancelMessage(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('createMessage', function() {
      it('should call createMessage successfully', function(done) {
        //uncomment below and update the code to test createMessage
        //instance.createMessage(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('createPricing', function() {
      it('should call createPricing successfully', function(done) {
        //uncomment below and update the code to test createPricing
        //instance.createPricing(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('deleteMessage', function() {
      it('should call deleteMessage successfully', function(done) {
        //uncomment below and update the code to test deleteMessage
        //instance.deleteMessage(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('getMessage', function() {
      it('should call getMessage successfully', function(done) {
        //uncomment below and update the code to test getMessage
        //instance.getMessage(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('getNetwork', function() {
      it('should call getNetwork successfully', function(done) {
        //uncomment below and update the code to test getNetwork
        //instance.getNetwork(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('getPricing', function() {
      it('should call getPricing successfully', function(done) {
        //uncomment below and update the code to test getPricing
        //instance.getPricing(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('listMessages', function() {
      it('should call listMessages successfully', function(done) {
        //uncomment below and update the code to test listMessages
        //instance.listMessages(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('listNetworks', function() {
      it('should call listNetworks successfully', function(done) {
        //uncomment below and update the code to test listNetworks
        //instance.listNetworks(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('sendMessage', function() {
      it('should call sendMessage successfully', function(done) {
        //uncomment below and update the code to test sendMessage
        //instance.sendMessage(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
  });

}));
