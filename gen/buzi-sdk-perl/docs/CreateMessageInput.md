# Buzi::V1::Object::CreateMessageInput

## Load the model package
```perl
use Buzi::V1::Object::CreateMessageInput;
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**to** | **ARRAY[string]** |  | [optional] 
**from** | **string** |  | [optional] 
**network_id** | **int** |  | [optional] 
**callback_url** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


