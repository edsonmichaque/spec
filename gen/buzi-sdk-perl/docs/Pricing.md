# Buzi::V1::Object::Pricing

## Load the model package
```perl
use Buzi::V1::Object::Pricing;
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cost_per_unit** | **double** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


