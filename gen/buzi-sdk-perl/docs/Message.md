# Buzi::V1::Object::Message

## Load the model package
```perl
use Buzi::V1::Object::Message;
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional] 
**created_at** | **DATE_TIME** |  | [optional] 
**status** | **string** |  | [optional] 
**reason** | **string** |  | [optional] 
**cost** | [**Cost**](Cost.md) |  | [optional] 
**inbox** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


