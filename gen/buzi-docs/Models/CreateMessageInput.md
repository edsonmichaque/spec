# CreateMessageInput
## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
| **to** | **List** |  | [optional] [default to null] |
| **from** | **String** |  | [optional] [default to null] |
| **network\_id** | **Integer** |  | [optional] [default to null] |
| **callback\_url** | **String** |  | [optional] [default to null] |

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

