# Pricing
## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
| **cost\_per\_unit** | **BigDecimal** |  | [optional] [default to null] |

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

