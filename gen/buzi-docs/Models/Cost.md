# Cost
## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
| **currency** | **String** |  | [optional] [default to null] |
| **value** | **BigDecimal** |  | [optional] [default to null] |

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

