# Message
## Properties

| Name | Type | Description | Notes |
|------------ | ------------- | ------------- | -------------|
| **id** | **UUID** |  | [optional] [default to null] |
| **created\_at** | **Date** |  | [optional] [default to null] |
| **status** | **String** |  | [optional] [default to null] |
| **reason** | **String** |  | [optional] [default to null] |
| **cost** | [**Cost**](Cost.md) |  | [optional] [default to null] |
| **inbox** | **String** |  | [optional] [default to null] |

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

