# openapi.model.CreateMessageInput

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**to** | **List<String>** |  | [optional] [default to const []]
**from** | **String** |  | [optional] 
**networkId** | **int** |  | [optional] 
**callbackUrl** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


