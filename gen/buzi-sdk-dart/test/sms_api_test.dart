//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.12

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: constant_identifier_names
// ignore_for_file: lines_longer_than_80_chars

import 'package:openapi/api.dart';
import 'package:test/test.dart';


/// tests for SmsApi
void main() {
  // final instance = SmsApi();

  group('tests for SmsApi', () {
    // Cancel a message
    //
    // Returns a single pet
    //
    //Future<Message> cancelMessage(int messageId) async
    test('test cancelMessage', () async {
      // TODO
    });

    // Create Message
    //
    // Update an existing pet by Id
    //
    //Future<Message> createMessage(CreateMessageInput createMessageInput) async
    test('test createMessage', () async {
      // TODO
    });

    // Create network price
    //
    // Returns a single pet
    //
    //Future<Message> createPricing(int networkId) async
    test('test createPricing', () async {
      // TODO
    });

    // Deletes a message
    //
    // delete a message
    //
    //Future<Error> deleteMessage(int messageId, { String apiKey }) async
    test('test deleteMessage', () async {
      // TODO
    });

    // Get message
    //
    // Returns a single pet
    //
    //Future<Message> getMessage(int messageId) async
    test('test getMessage', () async {
      // TODO
    });

    // Get network
    //
    // Returns a single pet
    //
    //Future<Network> getNetwork(int networkId, { int countryCode }) async
    test('test getNetwork', () async {
      // TODO
    });

    // List network rates
    //
    // Returns a single pet
    //
    //Future<List<Pricing>> getPricing(int networkId) async
    test('test getPricing', () async {
      // TODO
    });

    // List messages
    //
    // Update an existing pet by Id
    //
    //Future<List<Message>> listMessages({ String inbox, String status }) async
    test('test listMessages', () async {
      // TODO
    });

    // List networks
    //
    // Returns a single pet
    //
    //Future<List<Network>> listNetworks({ String countryCode }) async
    test('test listNetworks', () async {
      // TODO
    });

    // Sends a message
    //
    // Returns a single pet
    //
    //Future<Message> sendMessage(int messageId) async
    test('test sendMessage', () async {
      // TODO
    });

  });
}
