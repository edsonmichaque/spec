# Buzi::V1::Cost

## Properties

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **currency** | **String** |  | [optional] |
| **value** | **Float** |  | [optional] |

## Example

```ruby
require 'buzi-v1'

instance = Buzi::V1::Cost.new(
  currency: MZN,
  value: 1
)
```

