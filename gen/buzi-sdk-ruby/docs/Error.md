# Buzi::V1::Error

## Properties

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **code** | **Integer** |  | [optional] |
| **type** | **String** |  | [optional] |
| **message** | **String** |  | [optional] |

## Example

```ruby
require 'buzi-v1'

instance = Buzi::V1::Error.new(
  code: null,
  type: null,
  message: null
)
```

