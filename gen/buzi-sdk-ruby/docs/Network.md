# Buzi::V1::Network

## Properties

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **id** | **Integer** |  | [optional] |
| **created_at** | **Integer** |  | [optional] |

## Example

```ruby
require 'buzi-v1'

instance = Buzi::V1::Network.new(
  id: null,
  created_at: null
)
```

