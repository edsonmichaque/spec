# Buzi::V1::Pricing

## Properties

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **cost_per_unit** | **Float** |  | [optional] |

## Example

```ruby
require 'buzi-v1'

instance = Buzi::V1::Pricing.new(
  cost_per_unit: null
)
```

