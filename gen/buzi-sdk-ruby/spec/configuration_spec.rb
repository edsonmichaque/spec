=begin
#Swagger Petstore - OpenAPI 3.0

#This is a sample Pet Store Server based on the OpenAPI 3.0 specification.  You can find out more about Swagger at [https://swagger.io](https://swagger.io). In the third iteration of the pet store, we've switched to the design first approach! You can now help us improve the API whether it's by making changes to the definition itself or to the code. That way, with time, we can improve the API in general, and expose some of the new features in OAS3.  _If you're looking for the Swagger 2.0/OAS 2.0 version of Petstore, then click [here](https://editor.swagger.io/?url=https://petstore.swagger.io/v2/swagger.yaml). Alternatively, you can load via the `Edit > Load Petstore OAS 2.0` menu option!_  Some useful links: - [The Pet Store repository](https://github.com/swagger-api/swagger-petstore) - [The source API definition for the Pet Store](https://github.com/swagger-api/swagger-petstore/blob/master/src/main/resources/openapi.yaml)

The version of the OpenAPI document: 1.0.0
Contact: edson@michaque.com
Generated by: https://openapi-generator.tech
OpenAPI Generator version: 6.1.1-SNAPSHOT

=end

require 'spec_helper'

describe Buzi::V1::Configuration do
  let(:config) { Buzi::V1::Configuration.default }

  before(:each) do
    # uncomment below to setup host and base_url
    # require 'URI'
    # uri = URI.parse("https://petstore3.swagger.io")
    # Buzi::V1.configure do |c|
    #   c.host = uri.host
    #   c.base_url = uri.path
    # end
  end

  describe '#base_url' do
    it 'should have the default value' do
      # uncomment below to test default value of the base path
      # expect(config.base_url).to eq("https://petstore3.swagger.io")
    end

    it 'should remove trailing slashes' do
      [nil, '', '/', '//'].each do |base_url|
        config.base_url = base_url
        # uncomment below to test trailing slashes
        # expect(config.base_url).to eq("https://petstore3.swagger.io")
      end
    end
  end
end
