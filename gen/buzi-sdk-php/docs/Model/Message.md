# # Message

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | [optional]
**createdAt** | **\DateTime** |  | [optional]
**status** | **string** |  | [optional]
**reason** | **string** |  | [optional]
**cost** | [**\Buzi\V1\Model\Cost**](Cost.md) |  | [optional]
**inbox** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
