# # CreateMessageInput

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**to** | **string[]** |  | [optional]
**from** | **string** |  | [optional]
**networkId** | **int** |  | [optional]
**callbackUrl** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
