#!/usr/bin/env bash

DATE=$(date +'%Y%m%d%H%M')
cp -rf configs configs-$DATE

if [[ -d gen ]]; then
   rm -rf gen
fi

PROJECTS="docs sdk-python sdk-go sdk-java sdk-js sdk-ruby sdk-rust sdk-perl sdk-net sdk-c spec sdk-swift sdk-dart cli"
for i in $PROJECTS; do
    git clone git@gitlab.com:buzi-project/buzi-$i.git gen/buzi-$i
done


function generate() {
    BUILD=0
    if [[ -f gen/$2/BUILD ]]; then
        OLD_MINOR=$(cat gen/$2/BUILD | cut -d '.' -f 2)
        OLD_BUILD=$(cat gen/$2/BUILD | cut -d '.' -f 3)
        if [ "$OLD_MINOR" == "$DATE" ]; then
            BUILD=$((OLD_BUILD + 1))
        fi
    fi

    sed -i "s|TIMESTAMP|$DATE|g" configs-$DATE/*.json
    sed -i "s|BUILD|$BUILD|g" configs-$DATE/*.json


    docker run --rm -v "${PWD}:/local:Z" --user "$(id -u):$(id -g)" openapitools/openapi-generator-cli generate \
        -i  /local/$3 \
        -g $1 \
        -o /local/gen/$2 \
        -c /local/configs-$DATE/$1.json \
        -t /local/templates/$1 \
        --api-name-suffix \
        --remove-operation-prefix \
        --api-name-suffix ''

     echo 0.$DATE.$BUILD > gen/$2/BUILD
     sed  -i 's/GIT_USER_ID/buzi-project/g' **/*/*.md
     sed  -i "s/GIT_REPO_ID/$2/g" **/*/*.md

     cp LICENSE-$4 gen/$2/LICENSE
}

generate java buzi-sdk-java openapi.yml Apache-2.0
generate rust buzi-sdk-rust openapi.yml Apache-2.0
generate csharp-netcore buzi-sdk-net openapi.yml Apache-2.0
generate ruby buzi-sdk-ruby openapi.yml Apache-2.0
generate python buzi-sdk-python openapi.yml Apache-2.0
generate php buzi-sdk-php openapi.yml Apache-2.0
generate javascript buzi-sdk-js openapi.yml Apache-2.0
generate go buzi-sdk-go openapi.yml Apache-2.0
generate perl buzi-sdk-perl openapi.yml Apache-2.0
generate markdown buzi-docs openapi.yml Apache-2.0
generate dart buzi-sdk-dart openapi.yml Apache-2.0
generate swift5 buzi-sdk-swift openapi.yml Apache-2.0

sed  -r -i 's|json:"(.*)"|json:"\1" yaml:"\1"|g' gen/**/model_*.go

rm -rf configs-$DATE

PARENT=$(pwd)
for i in $(ls gen); do
    cd gen/$i
    git add :/
    git commit -m "Build $DATE"
    git push origin develop
    cd $PARENT
done
